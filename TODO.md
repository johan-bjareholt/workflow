0.2.2
* [x] better logo
* [x] about dialog
* [x] use queries instead of parsing raw data
* [ ] calendar "start on sunday" bugfix

* [ ] preferences dialog for AW
* [ ] GUI+automatic (from desktop/$XDG_SESSION_TYPE) bucket chooser
* [ ] auto detect app name/icon from reverse .desktop
* [ ] control start/stop of AW
* [ ] async updates -> don't clog the UI thread
* [ ] graph update transition/animation
* [ ] support for translations
* [ ] use proper .ui files
* [ ] implement trivial caching (e.g. 3600s TTL for weekly events, 30s for daily usage), combine with auto-sync and UI update
