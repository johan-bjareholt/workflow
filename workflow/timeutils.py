from datetime import datetime, date, timedelta, timezone

RFC3339_UTC_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

def format_rfc3339(date):
    date_utc = date.astimezone(timezone.utc)
    date_str = date.strftime(RFC3339_UTC_FORMAT)
    return date_str

def format_seconds(seconds):
    total_seconds = int(seconds)

    hours = int(seconds / 3600)
    minutes = int(seconds / 60) - 60 * hours
    seconds = total_seconds % 60

    if total_seconds < 60:
        return str(seconds) + "s"
    elif total_seconds < 3600:
        return str(minutes) + "m " + str(seconds) + "s"
    else:
        return str(hours) + "h " + str(minutes) + "m " # + str(seconds) + "s"

def find_week_first(date):
    return date - timedelta(days=date.weekday())

def find_week_last(date):
    return find_week_first(date) + timedelta(days=6)
