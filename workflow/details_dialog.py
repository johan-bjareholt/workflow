from gi.repository import Gtk
from . import timeutils

class DetailsDialog(Gtk.Window):
    ranking = []
    def __init__(self, ranking, **kwargs):
        super().__init__(**kwargs)
        self.ranking = ranking
        self.set_default_size(500,300)

        record_listbox = Gtk.ListBox()
        record_listbox.set_selection_mode(Gtk.SelectionMode.NONE)

        if len(self.ranking) == 1 and list(self.ranking.keys())[0] == "excluded":
            # tracking is disabled for this item -> show warning
            label = Gtk.Label("Window title tracking disabled")
            record_listbox.set_placeholder(label)
            label.set_visible(True)
        else:
            for title, time in sorted(self.ranking.items(), key=lambda item: item[1], reverse=True):
                row = Gtk.ListBoxRow()
                v_box = Gtk.Box(spacing=0, margin=15, orientation=Gtk.Orientation.HORIZONTAL)

                label = Gtk.Label()
                label.use_markup = True
                label.set_markup("<span foreground=\"grey\"><i>" + timeutils.format_seconds(time) + "</i></span>")
                label.set_size_request(80,1)
                label.hexpand = False
                label.halign = Gtk.Align.START
                v_box.pack_start(label, False, False , 10)

                label = Gtk.Label(title)
                label.set_ellipsize(True)
                v_box.pack_start(label, False, True, 10)

                row.add(v_box)
                record_listbox.add(row)

        a = Gtk.ScrolledWindow()
        a.add(record_listbox)
        self.add(a)
        record_listbox.show_all()
        self.show_all()

