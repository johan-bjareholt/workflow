#!@PYTHON@

# Needed for running standalone Workflow as a module outside Flatpak
# Currently unneeded code has been commented out to allow for (hackish) standalone execution

import os
import sys
import signal
import gettext

VERSION = '@VERSION@'
pkgdatadir = '@pkgdatadir@'
localedir = '@localedir@'


sys.path.insert(1, pkgdatadir)
signal.signal(signal.SIGINT, signal.SIG_DFL)
#gettext.install('workflow', localedir)

if __name__ == '__main__':
    import gi

    from gi.repository import Gio
    #resource = Gio.Resource.load(os.path.join(pkgdatadir, 'workflow.gresource'))
    #resource._register()

    from workflow import main
    sys.exit(main.main(VERSION))
